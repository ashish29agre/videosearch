package com.wooqer.videosearch.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.wooqer.videosearch.R;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */
public class NetworkManager {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    private static Context mContext;

    private NetworkStatusChangeListener mNetworkStatusChangeListener;

    public NetworkManager(Context context) {
        mContext = context;
    }

    public NetworkManager(Context context, NetworkStatusChangeListener networkStatusChangeListener) {
        mContext = context;
        mNetworkStatusChangeListener = networkStatusChangeListener;
    }

    public int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        mContext = context;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public int getConnectivityStatusString(Context context) {
        int connectivityStatus = getConnectivityStatus(context);
        if (connectivityStatus == NetworkManager.TYPE_NOT_CONNECTED) {
            Toast.makeText(mContext, R.string.no_network_available, Toast.LENGTH_SHORT).show();
            if (mNetworkStatusChangeListener != null)
                mNetworkStatusChangeListener.networkOffline();
        } else {
            if (mNetworkStatusChangeListener != null)
                mNetworkStatusChangeListener.networkOnline();
        }
        return connectivityStatus;
    }

    public interface NetworkStatusChangeListener {
        void networkOffline();
        void networkOnline();
    }
}
